const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const cors = require('cors');
const fs = require('fs');
var path = require('path')
const session = require('express-session');
const router = require('./app/controllers/router');
const port = 3000;

let mongoConnection = "mongodb+srv://admin:aeoea123@marketautos.7pedzow.mongodb.net/MarketAutosDB";
let db = mongoose.connection;


app.use(router);
app.use(express.static('app'));
app.use('/views', express.static('views'));
app.use(express.static(path.join(__dirname, 'app/public')));
app.use(express.json());
//Body parser
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors({
    methods: ['GET', 'POST', 'DELETE', 'UPDATE', 'PUT', 'PATCH']
}));
//las sesiones se guardan del lado del server

app.use(session({
    //secret es la string secreta para firmar la sesion
    secret: 'ultracarsSecret',
    resave: true,
    saveUninitialized: true
  }));  

db.on('connecting', () => {
    console.log("Cargando la base de datos...");
});
db.on('connected', () => {
    console.log("Se ha cargado la base de datos.");
});
mongoose.connect(mongoConnection, { useNewUrlParser: true });

let carSchema = new mongoose.Schema({
    marca: {
        type: String,
        required: true
    },
    tipo: {
        type: String,
        required: true
    },
    modelo: {
        type: String,
        required: true
    },
    color: {
        type: String,
        required: true
    },
    precio: {
        type: Number,
        required: true
    },
    año: {
        type: Number,
        min: 1886,
        max: 2023,
        required: true
    },
    image: {
        type: String,
        required: true
    },
    matricula: {
        type: String,
        required: true
    },
    descripcion: {
        type: String,
        required: true
    },
    autor: {
        type: String,
        required: true
    }
});

let userSchema = new mongoose.Schema({
    username: {
        type: String,
        required: true,
        unique: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    contraseña: {
        type: String,
        required: true
    },
    token: {
        type: String,
    },
    profilePic: {
        type: String
    },
    telefono: {
        type: String
    }
});

//antes de que se guarden los datos hasheamos contra
userSchema.pre('save', function (next) {
    if (this.isNew || this.isModified('contraseña')) {
        const document = this;
        bcrypt.hash(document.contraseña, 10, (err, hashedContraseña) => {
            if (err) {
                next(err);
            } else {
                document.contraseña = hashedContraseña;
                next();
            }
        });
    } else {
        next();
    }
});

userSchema.methods.isCorrectPassword = function (contraseña, callback) {
    bcrypt.compare(contraseña, this.contraseña, function (err, same) {
        if (err) {
            callback(err);
        } else {
            callback(err, same);
        }
    });
}

let compradosSchema = new mongoose.Schema({
    marca: {
        type: String,
        required: true
    },
    tipo: {
        type: String,
        required: true
    },
    modelo: {
        type: String,
        required: true
    },
    color: {
        type: String,
        required: true
    },
    precio: {
        type: Number,
        required: true
    },
    año: {
        type: Number,
        min: 1886,
        max: 2023,
        required: true
    },
    image: {
        type: String,
        required: true
    },
    matricula: {
        type: String,
        required: true
    },
    descripcion: {
        type: String,
        required: true
    },
    autor: {
        type: String,
        required: true
    }
});
let productosGuardadosSchema = new mongoose.Schema({
    marca: {
        type: String,
        required: true
    },
    tipo: {
        type: String,
        required: true
    },
    modelo: {
        type: String,
        required: true
    },
    color: {
        type: String,
        required: true
    },
    precio: {
        type: Number,
        required: true
    },
    año: {
        type: Number,
        min: 1886,
        max: 2023,
        required: true
    },
    image: {
        type: String,
        required: true
    },
    matricula: {
        type: String,
        required: true
    },
    descripcion: {
        type: String,
        required: true
    },
    autor: {
        type: String,
        required: true
    }
});
let publicidadSchema = new mongoose.Schema({
    marca: {
        type: String,
        required: true
    },
    tipo: {
        type: String,
        required: true
    },
    modelo: {
        type: String,
        required: true
    },
    color: {
        type: String,
        required: true
    },
    precio: {
        type: Number,
        required: true
    },
    año: {
        type: Number,
        min: 1886,
        max: 2023,
        required: true
    },
    image: {
        type: String,
        required: true
    },
    matricula: {
        type: String,
        required: true
    },
    descripcion: {
        type: String,
        required: true
    },
    autor: {
        type: String,
        required: true
    }
});

let Publicidad = mongoose.model('publicidads', publicidadSchema);
let ProductosGuardados = mongoose.model('guardados', productosGuardadosSchema);
let Comprados = mongoose.model('comprados', compradosSchema);
let Car = mongoose.model('carros', carSchema);
let User = mongoose.model('usuarios', userSchema);

// registrar productos/usuarios
app.post('/products', async (req, res) => {
    console.log("Agregando carro...");
    try {
        const car = new Car(req.body);
        await car.save();
        res.status(201).send("Se agrego el carro");
        console.log("Carro agregado");
    } catch (err) {
        console.error(err);
        res.status(500).json("Error al agregar el carro");
    }
});
app.get('/products/:matricula', async (req, res) => {
    console.log("Buscando carro...");
    try {
        const carFilter = req.params.matricula;
        const cars = await Car.find({ matricula: carFilter });
        console.log("Carro encontrado");
        res.status(201).send(cars);
    } catch (err) {
        console.log(err);
        res.status(500).send("Error al filtrar carro");
    }
});
//regresar general
app.get('/products', async (req, res) => {
    console.log("Buscando todos los productos...");
    try {
        const cars = await Car.find();
        console.log(`Se encontraron ${cars.length} productos`);
        res.status(200).send(cars);
    } catch (error) {
        console.error(error);
        res.status(500).send("Error al buscar productos");
    }
});

app.put('/products/:matricula', async (req, res) => {
    console.log("Actualizando vehículo...")
    try {
        const carMatricula = req.params.matricula;
        const { marca, tipo, modelo, color, precio, año, image } = req.body;
        const car = await Car.findOneAndUpdate({ matricula: carMatricula }, { marca, tipo, modelo, color, precio, año, image }, { new: true });
        if (!car) {
            return res.status(404).send('Vehículo no encontrado');
        }
        console.log("Vehículo actualizado:", car);
        res.send(car);
    } catch (error) {
        console.error(error);
        res.status(500).send('Error al actualizar vehículo');
    }
});
app.delete('/products/:matricula', async (req, res) => {
    const { matricula } = req.params;
    try {
        const deletedProduct = await Car.deleteOne({ matricula: matricula });
        if (deletedProduct) {
            res.status(200).send('Producto eliminado');
        } else {
            res.status(404).send('Producto no encontrado');
        }
    } catch (error) {
        console.error(error);
        res.status(500).send('Error al eliminar Producto');
    }
});
//---------Users-----------------------------------------------------------------------------------------------------------------//
app.post('/users', async (req, res) => {
    console.log("Registrando usuario...");
    try {
        const user = new User({
            username: req.body.username,
            email: req.body.email,
            contraseña: req.body.contraseña
        });
        await user.save();
        console.log("Usuario creado con exito");
        res.redirect('/');
    } catch (err) {
        console.error(err);
        res.status(500).json("Error al registrar el usuario");
    }
});

app.post('/Login', async (req, res) => {
    try {
        const { username, contraseña } = req.body;
        User.findOne({ username })
            .then(user => {
                if (!user) {
                    res.status(500).json("El usuario no existe");
                } else {
                    user.isCorrectPassword(contraseña, (err, result) => {
                        if (err) {
                            res.status(500).json("Error al validar el usuario");
                        } else if (result) {
                            // Usuario autenticado correctamente
                            req.session.username = username;
                            console.log("SESSION " + req.session.username);
                            req.session.save();
                            res.redirect('/home');
                        } else {
                            res.status(500).json("Usuario o contraseña incorrectos");
                        }
                    });
                }
            })
            .catch(err => {
                res.status(500).json("Error al validar el usuario");
            });

    } catch (err) {
        console.error(err);
        res.status(500).json("Error al ingresar");
    }
});


// regresar por token/id
app.get('/users/:token', async (req, res) => {
    console.log("Buscando usuario...");
    try {
        const userFilter = req.params.token;
        const users = await User.find({ token: userFilter });
        console.log("Usuario encontrado");
        res.status(201).send(users);
    } catch (err) {
        console.log(err);
        res.status(500).send("Error al filtrar usuario");
    }
});
app.put('/users/:token', async (req, res) => {
    console.log("Actualizando usuario...")
    try {
        const userToken = req.params.token;
        const { username, email, contraseña, profilePic } = req.body;
        const user = await User.findOneAndUpdate({ token: userToken }, { username, email, contraseña, profilePic }, { new: true });
        if (!user) {
            return res.status(404).send('Usuario no encontrado');
        }
        res.send(user);
        console.log("Se actualizó el usuario");
    } catch (error) {
        console.error(error);
        res.status(500).send('Error al actualizar usuario');
    }
});
// borrar usuarios
app.delete('/users/:token', async (req, res) => {
    const { token } = req.params;
    try {
        const deletedProduct = await User.deleteOne({ token: token });
        if (deletedProduct) {
            res.status(200).send('Usuario eliminado');
        } else {
            res.status(404).send('Usuario no encontrado');
        }
    } catch (error) {
        console.error(error);
        res.status(500).send('Error al eliminar Usuario');
    }
});
//--------------------------------------------------------------------------------------------------------------------------//
app.post('/comprar/:matricula', async (req, res) => {
    console.log("Comprando carro...");
    try {
        const carMatricula = req.params.matricula;
        const car = await Car.findOne({ matricula: carMatricula });
        if (!car) {
            return res.status(404).send('Carro no encontrado');
        }
        const comprado = new Comprados(car.toObject());
        await comprado.save();
        await Car.deleteOne({ matricula: carMatricula }); //se borra de la tabla products porque ya fue comprado y no existe
        console.log("Carro comprado y movido a la lista de comprados");
        res.status(201).send("Carro comprado");
    } catch (err) {
        console.error(err);
        res.status(500).json("Error al comprar el carro");
    }
});
app.get('/comprados', async (req, res) => {
    try {
        const comprados = await Comprados.find({});
        res.status(200).json(comprados);
    } catch (err) {
        console.error(err);
        res.status(500).json("Error al obtener los productos comprados");
    }
});
app.delete('/comprados/:matricula', async (req, res) => {
    try {
        const carMatricula = req.params.matricula;
        await Comprados.deleteOne({ matricula: carMatricula });
        console.log("Carro eliminado de la lista de productos comprados");
        res.status(200).send("Carro eliminado de la lista de productos comprados");
    } catch (err) {
        console.error(err);
        res.status(500).json("Error al eliminar el carro de la lista de productos comprados");
    }
});

//--------------------------------------------------------------------------------------------------------------------------//
app.post('/guardados/:matricula', async (req, res) => {
    console.log("Guardando carro...");
    try {
        const carMatricula = req.params.matricula;
        const car = await Car.findOne({ matricula: carMatricula });
        if (!car) {
            return res.status(404).send('Carro no encontrado');
        }
        const guardado = new ProductosGuardados(car.toObject());
        await guardado.save();
        console.log("Carro guardado en la lista de productos guardados");
        res.status(201).send("Carro guardado");
    } catch (err) {
        console.error(err);
        res.status(500).json("Error al guardar el carro");
    }
});
app.get('/guardados', async (req, res) => {
    try {
        const guardados = await ProductosGuardados.find({});
        res.status(200).json(guardados);
    } catch (err) {
        console.error(err);
        res.status(500).json("Error al obtener los productos guardados");
    }
});
app.delete('/guardados/:matricula', async (req, res) => {
    try {
        const carMatricula = req.params.matricula;
        await ProductosGuardados.deleteOne({ matricula: carMatricula });
        console.log("Carro eliminado de la lista de productos guardados");
        res.status(200).send("Carro eliminado de la lista de productos guardados");
    } catch (err) {
        console.error(err);
        res.status(500).json("Error al eliminar el carro de la lista de productos guardados");
    }
});

//--------------------------------------------------------------------------------------------------------------------------//
app.post('/publicidad/:matricula', async (req, res) => {
    console.log("Agregando carro a la lista de publicidad...");
    try {
        const carMatricula = req.params.matricula;
        const car = await Car.findOne({ matricula: carMatricula });
        if (!car) {
            return res.status(404).send('Carro no encontrado');
        }
        const publicidad = new Publicidad(car.toObject());
        await publicidad.save();
        console.log("Carro agregado a la lista de publicidad");
        res.status(201).send("Carro agregado a la lista de publicidad");
    } catch (err) {
        console.error(err);
        res.status(500).json("Error al agregar el carro a la lista de publicidad");
    }
});
app.get('/publicidad', async (req, res) => {
    try {
        const publicidades = await Publicidad.find({});
        res.status(200).json(publicidades);
    } catch (err) {
        console.error(err);
        res.status(500).json("Error al obtener los productos de publicidad");
    }
});
app.delete('/publicidad/:matricula', async (req, res) => {
    try {
        const carMatricula = req.params.matricula;
        await Publicidad.deleteOne({ matricula: carMatricula });
        console.log("Carro eliminado de la lista de publicidad");
        res.status(200).send("Carro eliminado de la lista de publicidad");
    } catch (err) {
        console.error(err);
        res.status(500).json("Error al eliminar el carro de la lista de publicidad");
    }
});

//rutas con info del user
app.get('/user_info', (req, res) => {
    res.send(req.session.username);
  });

  app.get('/test', (req, res) => {
    res.send(req.session.username);
  });

app.listen(port, () => {
    console.log("Ultracars corriendo en el puerto " + port);
});


