// Obtener los productos publicitados
let xhr3 = new XMLHttpRequest();
let url3 = "http://localhost:3000/publicidad";
let divPublicidad = document.querySelector('.publicidad');

document.addEventListener('DOMContentLoaded', () => {
  xhr3.open('GET', url3);
  xhr3.setRequestHeader("content-type", "application/json");
  xhr3.onload = function () {
    if (xhr3.status === 200) {
      var publicidades = JSON.parse(xhr3.responseText);
      var randomIndex = Math.floor(Math.random() * publicidades.length);
      var publicidad = publicidades[randomIndex];
      divPublicidad.innerHTML = `
        <div class="card text-start h-100">
          <img class="card-img-top" src="${publicidad.image}" alt="${publicidad.modelo}">
          <div class="card-body">
            <h4 class="card-title">${publicidad.marca} ${publicidad.modelo}</h4>
            <p>${publicidad.color}</p>
            <p>${publicidad.año}</p>
            <p>${publicidad.precio}</p>
          </div>
          <div class="card-footer">
            <a href = "auto2"><button type="button" class="card_buy_car_btn" data-id="${publicidad.matricula}">Comprar</button></a>
          </div>
        </div>
      `;
      let carBtn = document.querySelector(".card_buy_car_btn");
      carBtn.addEventListener("click", () => {
        sessionStorage.setItem("vender", JSON.stringify(publicidad));
      });
    } else {
      console.log("Error en la base de datos");
    }
  }
  xhr3.send();
});
function generateMatricula() { 
  const letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  const numbers = '0123456789';
  let licensePlate = '';
  for (let i = 0; i < 3; i++) {
    licensePlate += letters.charAt(Math.floor(Math.random() * letters.length));
  }
  for (let i = 0; i < 4; i++) {
    licensePlate += numbers.charAt(Math.floor(Math.random() * numbers.length));
  }
  return licensePlate; 
}

document.getElementById("publish-btn").addEventListener("click", () => {
  let authorName = document.getElementById("username");
  let newCar = {
    marca : document.getElementById("marca").value,
    tipo: document.getElementById("tipo").value,
    modelo: document.getElementById("modelo").value,
    color: document.getElementById("color").value,
    precio: document.getElementById("precio").value,
    año: document.getElementById("año").value,
    image: document.getElementById("foto").value,
    matricula: generateMatricula(),
    descripcion: document.getElementById("descripcion").value,
    autor: authorName.innerHTML
  }
  let carsUrl = "http://localhost:3000/products";
  xhr3.open('POST', carsUrl);
  xhr3.setRequestHeader("content-type", "application/json");
  xhr3.send(JSON.stringify(newCar));
});