let url = "http://localhost:3000/comprar/:matricula";
let xhr = new XMLHttpRequest();

var testobj = {
    marca: "Toyotaa",
    tipo: "Todoterreno",
    modelo: "Highlander",
    color: "Blanco",
    precio: 366202,
    año: 2023,
    image: "https://cdn.carbuzz.com/gallery-images/2017-toyota-highlander-hybrid-carbuzz-391672-1600.jpg",
    matricula: "82635"
    
}

document.getElementById("car-buybtn").addEventListener("click", async () => {
    xhr.open('POST', url, true);  
    xhr.setRequestHeader('Content-type', 'application/json');
    console.log("Enviando compra...");
    await xhr.send(JSON.stringify(testobj));
    console.log("Compra enviada");
});