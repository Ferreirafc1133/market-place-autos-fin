// Obtener los productos gpublicitados
let xhr3 = new XMLHttpRequest();
let url3 = "http://localhost:3000/publicidad";
let divPublicidad = document.querySelector('.publicidad');

document.addEventListener('DOMContentLoaded', () => {
  xhr3.open('GET', url3);
  xhr3.setRequestHeader("content-type", "application/json");
  xhr3.onload = function () {
    if (xhr3.status === 200) {
      var publicidades = JSON.parse(xhr3.responseText);
      var publicidadesAleatorias = obtenerPublicidadesAleatorias(publicidades, 3); // Selecciona 3 publicidades aleatorias
      var divsPublicidad = document.querySelectorAll('.publicidad'); // Selecciona las 3 divs con la clase publicidad
      for (var i = 0; i < divsPublicidad.length; i++) {
        var publicidad = publicidadesAleatorias[i]; // Selecciona la publicidad correspondiente
        divsPublicidad[i].innerHTML = `
          <div class="card text-start h-100">
            <img class="card-img-top" src="${publicidad.image}" alt="${publicidad.modelo}">
            <div class="card-body">
              <h4 class="card-title">${publicidad.marca} ${publicidad.modelo}</h4>
              <p>${publicidad.color}</p>
              <p>${publicidad.año}</p>
              <p>${publicidad.precio}</p>
            </div>
            <div class="card-footer">
              <a href = "auto"><button type="button" class="card_buy_car_btn" data-id="${publicidad.matricula}">Comprar</button></a>
            </div>
          </div>
        `;

        let carBtns = document.querySelectorAll(".card_buy_car_btn");
        carBtns.forEach((carBtn, i) => {
          carBtn.addEventListener("click", () => {
            let publicidad = publicidadesAleatorias[i];
            sessionStorage.setItem("publi" + (i + 1), JSON.stringify(publicidad));
          });
        });
      }
    } else {
      console.log("Error en la base de datos");
    }
  }
  xhr3.send();
});

function obtenerPublicidadesAleatorias(publicidades, cantidad) {
  var publicidadesAleatorias = [];
  var publicidadesDisponibles = publicidades.slice(); // Crear una copia de la lista de publicidades
  for (var i = 0; i < cantidad; i++) {
    if (publicidadesDisponibles.length === 0) {
      break; // Si no hay más publicidades disponibles, salir del ciclo
    }
    var randomIndex = Math.floor(Math.random() * publicidadesDisponibles.length);
    var publicidad = publicidadesDisponibles[randomIndex];
    publicidadesAleatorias.push(publicidad);
    publicidadesDisponibles.splice(randomIndex, 1); // Eliminar la publicidad seleccionada de la lista de publicidades disponibles
  }
  return publicidadesAleatorias;
}
//----------------------------------detalle del auto--------------------------
// Verificar si la información del automóvil se guardó con la clave "carrito" o "vender"
carInfo = JSON.parse(sessionStorage.getItem("vender"));

// Actualizar los elementos HTML con la información del automóvil
document.getElementById("car-image").src = carInfo.image;
document.getElementById("car-model").innerText = carInfo.modelo;
document.getElementById("car-description").innerText = carInfo.descripcion;
document.getElementById("car-price").innerText = carInfo.precio.toLocaleString('es-AR', { style: 'currency', currency: 'ARS' });

document.getElementById("fav-btn").addEventListener("click", () => {
  let carData = JSON.parse(sessionStorage.getItem('carrito'));
  let carId = carData.matricula
  let favUrl = "http://localhost:3000/guardados/" + carId;

  xhr3.open('POST', favUrl);
  xhr3.setRequestHeader("content-type", "application/json");
  xhr3.send(JSON.stringify(carData));
});
document.getElementById("car-buybtn").addEventListener("click", () => {
  let carData = JSON.parse(sessionStorage.getItem('carrito'));
  let carId = carData.matricula
  let favUrl = "http://localhost:3000/comprar/" + carId;

  xhr3.open('POST', favUrl);
  xhr3.setRequestHeader("content-type", "application/json");
  xhr3.send(JSON.stringify(carData));
});