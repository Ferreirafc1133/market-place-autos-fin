// Obtener los productos comprados
let xhr = new XMLHttpRequest();
let url = "http://localhost:3000/comprados";
let divHistory = document.querySelector('.div-history');


// Obtener los productos guardados
let xhr2 = new XMLHttpRequest();
let url2 = "http://localhost:3000/guardados";
let divSave = document.querySelector('.div-save');

// Obtener los productos gpublicitados
let xhr3 = new XMLHttpRequest();
let url3 = "http://localhost:3000/publicidad";
let divPublicidad = document.querySelector('.publicidad');

let deleteBtnsBuy;
let deleteBtnsSave;

// Hacer la primera solicitud AJAX
document.addEventListener('DOMContentLoaded', () => {
    xhr.open('GET', url);
    xhr.setRequestHeader("content-type", "application/json");
    xhr.onload = function () {
        if (xhr.status === 200) {
            var history = JSON.parse(xhr.responseText);
            console.log(history);
            if (history == undefined || history.length == 0) {
                divHistory.innerHTML = '<h3 style="text-align: center;">Tu historial de compras esta vacio.</h3>';
            } else {
                divHistory.innerHTML = history.map(prod => `
                <div class="card-container">
                    <div class="card">
                        <div class="card-image"></div>
                        <div class="category"><img src="${prod.image}" alt="${prod.modelo}" class="CardsImg"></div>
                        <div class="heading">${prod.año} ${prod.marca} ${prod.modelo}</div>
                        <div class="author">By <span class="name">Maria</span> 2 days ago <a class="fancy delete-btn-buy" href="#">
                            <span class="top-key"></span>
                            <span class="text">Eliminar</span>
                            <span class="bottom-key-1"></span>
                            <span class="bottom-key-2"></span>
                            </a>
                        </div>
                    </div>
                `).join("");
                deleteBtnsBuy = document.getElementsByClassName("delete-btn-buy");
                for(let i = 0; i < deleteBtnsBuy.length; i++) {
                    deleteBtnsBuy[i].addEventListener("click", () => {
                        let carId = String(history[i].matricula);
                        let deleteUrl = "http://localhost:3000/comprados/" + carId;
                        xhr.open('DELETE', deleteUrl);
                        xhr.setRequestHeader("content-type", "application/json");
                        xhr.send();
                    });
                }
            }
        } else {
            console.log("Error en la base de datos");
        }
    }
   xhr.send();
});

// Hacer la segunda solicitud AJAX
document.addEventListener('DOMContentLoaded', () => {
    xhr2.open('GET', url2);
    xhr2.setRequestHeader("content-type", "application/json");
    xhr2.onload = function () {
        if (xhr2.status === 200) {
            var save = JSON.parse(xhr2.responseText);
            console.log(save);
            if (save == undefined || save.length == 0) {
                divSave.innerHTML = '<h3 style="text-align: center;">Tu historial de favoritos esta vacio.</h3>';
            } else {
                divSave.innerHTML = save.map(prod => `
                <div class="card-container">
                    <div class="card">
                        <div class="card-image"></div>
                        <div class="category"><img src="${prod.image}" alt="${prod.modelo}" class="CardsImg"></div>
                        <div class="heading">${prod.año} ${prod.marca} ${prod.modelo}</div>
                        <div class="author">By <span class="name">Maria</span> 2 days ago <a class="fancy delete-btn-save" href="#">
                            <span class="top-key"></span>
                            <span class="text">Eliminar</span>
                            <span class="bottom-key-1"></span>
                            <span class="bottom-key-2"></span>
                            </a>
                        </div>
                    </div>
                `).join("");
                deleteBtnsSave = document.getElementsByClassName("delete-btn-save");
                for(let i = 0; i < deleteBtnsSave.length; i++) {
                    deleteBtnsSave[i].addEventListener("click", () => {
                        let carId = String(save[i].matricula);
                        let deleteUrl = "http://localhost:3000/guardados/" + carId;
                        xhr.open('DELETE', deleteUrl);
                        xhr.setRequestHeader("content-type", "application/json");
                        xhr.send();
                    });    
                }
            }
        } else {
            console.log("Error en la base de datos");
        }
    }
   xhr2.send();
});


document.addEventListener('DOMContentLoaded', () => {
    xhr3.open('GET', url3);
    xhr3.setRequestHeader("content-type", "application/json");
    xhr3.onload = function () {
        if (xhr3.status === 200) {
            var publicidades = JSON.parse(xhr3.responseText);
            var randomIndex = Math.floor(Math.random() * publicidades.length);
            var publicidad = publicidades[randomIndex];
            divPublicidad.innerHTML = `
            <div class="card text-start h-100">
              <img class="card-img-top" src="${publicidad.image}" alt="${publicidad.modelo}">
              <div class="card-body">
                <h4 class="card-title">${publicidad.marca} ${publicidad.modelo}</h4>
                <p>${publicidad.año} `+`${publicidad.color}</p>
                <p>$`+`${publicidad.precio}</p>
              </div>
              <div class="card-footer">
              <a href = "auto3"><button type="button" class="card_buy_car_btn" data-id="${publicidad.matricula}">Comprar</button></a>
              </div>
            </div>
            `;
            let carBtn = document.querySelector(".card_buy_car_btn");
            carBtn.addEventListener("click", () => {
                sessionStorage.setItem("hist", JSON.stringify(publicidad));
            });
        } else {
            console.log("Error en la base de datos");
        }
    }
    xhr3.send();
});



