const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');

const router = express.Router();
app = express();

router.get('/', (req,res)=>res.sendFile(path.resolve(__dirname + "/../views/Login.html")));
router.get('/signup', (req,res)=>res.sendFile(path.resolve(__dirname + "/../views/signup.html")));
router.get('/home', (req,res)=>res.sendFile(path.resolve(__dirname + "/../views/home.html")));
router.get('/formulario', (req,res)=>res.sendFile(path.resolve(__dirname + "/../views/formulario.html")));
router.get('/Historial', (req,res)=>res.sendFile(path.resolve(__dirname + "/../views/Historial.html")));
router.get('/auto', (req,res)=>res.sendFile(path.resolve(__dirname + "/../views/auto.html")));
router.get('/auto1', (req,res)=>res.sendFile(path.resolve(__dirname + "/../views/auto1.html")));
router.get('/auto2', (req,res)=>res.sendFile(path.resolve(__dirname + "/../views/auto2.html")));
router.get('/auto3', (req,res)=>res.sendFile(path.resolve(__dirname + "/../views/auto3.html")));
router.get('/auto4', (req,res)=>res.sendFile(path.resolve(__dirname + "/../views/auto4.html")));
router.get('/auto5', (req,res)=>res.sendFile(path.resolve(__dirname + "/../views/auto5.html")));
router.get('/auto6', (req,res)=>res.sendFile(path.resolve(__dirname + "/../views/auto6.html")));
router.get('/auto7', (req,res)=>res.sendFile(path.resolve(__dirname + "/../views/auto7.html")));
router.get('/busqueda', (req,res)=>res.sendFile(path.resolve(__dirname + "/../views/busqueda.html")));

module.exports = router;