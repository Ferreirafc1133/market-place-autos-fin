const express = require('express');
const app = express();
const mongoose = require('mongoose');
const cors = require('cors');

let userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    token: {
        type: String,
    },
    tel: {
        type: String
    }
});

let collection = mongoose.model('users', userSchema);

app.post("/signup", async (req,res) => {
    const data ={
        name:req.body.name,
        password:req.body.password,
        email:req.body.email
    }
    await collection.insertMany([data]);
    res.render("home");
});

