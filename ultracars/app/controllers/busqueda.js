let xhr = new XMLHttpRequest();
let url = "http://localhost:3000/products"
let divSearch = document.querySelector('.div-search');
var matchingCars = [];

document.addEventListener('DOMContentLoaded', () => {
  xhr.open('GET', url);
  xhr.setRequestHeader("content-type", "application/json");
  xhr.onload = function () {
    if (xhr.status === 200) {
      var autos = JSON.parse(xhr.responseText);
      for (let i = 0; i < autos.length; i++) {
        if (autos[i].modelo.toLowerCase() == sessionStorage.getItem("search-term").toLowerCase()) {
          matchingCars.push(autos[i]);
        }
        if (matchingCars == undefined || matchingCars.length == 0) {
          divSearch.innerHTML = '<h1 style="text-align: center;">Ningun resultado coincide con tu busqueda</h1>';
        } else {
          divSearch.innerHTML = matchingCars.map(prod => `
          <div class="col-xl-3 col-lg-3 col-md-4 col-sm-12">
            <div class="cookie-card">
              <img class="card-img-top"
                src="${prod.image}"
                alt="${prod.modelo}">
              <span class="title">${prod.modelo}</span>
              <p class="description">${prod.descripcion}</p>
              <div class="card-footer">
                <div class="card-price">
                <span>${prod.precio.toLocaleString('es-AR', { style: 'currency', currency: 'ARS' })}</span>
              </div>
                <div class="card_actions">
                  <button class="card_seller">
                  ${prod.autor}
                  </button>
                  <a href="auto3"><button type="button" class="card_buy_car_btn" data-id="${prod.matricula}">Comprar</button></a>
                </div>
              </div>
            </div>
          </div>
        `).join("");
        }
      }
      let buyBtns = divSearch.querySelectorAll(".card_buy_car_btn");
      buyBtns.forEach((buyBtn, i) => {
        buyBtn.addEventListener("click", () => {
          sessionStorage.setItem("busqueda", JSON.stringify(matchingCars[i]));
        });
      });
    } else {
      console.log("Error en la base de datos");
    }
  }
  xhr.send();
});



