// Obtener los productos comprados
let xhr = new XMLHttpRequest();
let url = "http://localhost:3000/products";
let divautos = document.querySelector('.div-home');
let perPage = 8;
let favBtns;

// Hacer la primera solicitud AJAX
function renderAutos(autos) {
  let start = (currentPage - 1) * perPage;
  let end = start + perPage;
  let autosToShow = autos.slice(start, end);

  divautos.innerHTML = autosToShow.map((prod, i) => `
        <div class="col-xl-3 col-lg-3 col-md-4 col-sm-12">
          <div class="cookie-card">
            <img class="card-img-top"
              src="${prod.image}"
              alt="${prod.modelo}">
            <span class="title">${prod.modelo}</span>
            <p class="description">${prod.descripcion}</p>
            <div class="card-footer">
              <div class="card-price">
              <span>${prod.precio.toLocaleString('es-AR', { style: 'currency', currency: 'ARS' })}</span>
            </div>
              <div class="card_actions">
                <button class="card_seller">
                ${prod.autor}
                </button>
                <Button onclick="ToggleFav(this)" fav-data-id="${prod.matricula}" class="card_btn_fav"><i
                    class="fas fa-star"></i></Button>
                <a href="auto1"><button type="button" class="card_buy_car_btn" data-id="${prod.matricula}">Comprar</button></a>
              </div>
            </div>
          </div>
        </div>
      `).join("");

  let carBtns = divautos.querySelectorAll(".card_buy_car_btn");
  carBtns.forEach((carBtn, i) => {
    carBtn.addEventListener("click", () => {
      sessionStorage.setItem("carrito", JSON.stringify(autosToShow[i]));
    });
  });
  let favBtns = document.querySelectorAll('.card_btn_fav');
  favBtns.forEach((favBtn, i) => {
    favBtn.addEventListener("click", () => {
      console.log("Hello World!");
      console.log(autosToShow[i].matricula);
      let carId = String(autosToShow[i].matricula);
      if (favBtns[i].getAttribute("style") == "color: gold;") {
        let favUrl = "http://localhost:3000/guardados/" + carId;
        xhr.open('POST', favUrl);
        xhr.setRequestHeader("content-type", "application/json");
        xhr.send(JSON.stringify(autosToShow[i]));
      } else {
        let deleteUrl = "http://localhost:3000/guardados/" + carId;
        xhr.open('DELETE', deleteUrl);
        xhr.setRequestHeader("content-type", "application/json");
        xhr.send();
      }
    });
  });

  // Generar enlaces de paginación
  let totalPages = Math.ceil(autos.length / perPage);
  let pageLinks = "";
  for (let i = 1; i <= totalPages; i++) {
    pageLinks += `
        <li class="page-item${i === currentPage ? ' active' : ''}">
          <a class="page-link" href="#">${i}</a>
        </li>
      `;
  }

  // Actualizar los enlaces de paginación
  let pagination = document.querySelector(".pagination");
  pagination.innerHTML = `
      <li class="page-item${currentPage === 1 ? ' disabled' : ''}">
        <a class="page-link" href="#" aria-label="Previous">
          <span aria-hidden="true">&laquo;</span>
        </a>
      </li>
      ${pageLinks}
      <li class="page-item${currentPage === totalPages ? ' disabled' : ''}">
        <a class="page-link" href="#" aria-label="Next">
          <span aria-hidden="true">&raquo;</span>
        </a>
      </li>
    `;

  // Agregar eventos de clic a los enlaces de paginación
  let pageLinksEl = pagination.querySelectorAll(".page-link");
  pageLinksEl.forEach(link => {
    link.addEventListener("click", (event) => {
      event.preventDefault();
      let pageNumber = parseInt(event.target.innerText);
      if (pageNumber !== currentPage) {
        currentPage = pageNumber;
        renderAutos(autos);
      }
    });
  });
}
document.addEventListener('DOMContentLoaded', () => {
  xhr.open('GET', url);
  xhr.setRequestHeader("content-type", "application/json");
  xhr.onload = function () {
    if (xhr.status === 200) {
      var autos = JSON.parse(xhr.responseText);
      console.log(autos);
      if (autos == undefined || autos.length == 0) {
        divautos.innerHTML = '<h3 style="text-align: center;">Nos quedamos sin autos amiguitos.</h3>';
      } else {
        currentPage = 1; // Establece la página actual en 1
        renderAutos(autos); // Llama a la función renderAutos() para mostrar los elementos
        favBtns = document.getElementsByClassName('card_btn_fav');
        for(let i = 0; i < favBtns.length; i++) {
          favBtns[i].addEventListener("click", () => {
            let carId = String(autos[i].matricula);
            if(favBtns[i].getAttribute("style") == "color: gold;") {
              let favUrl = "http://localhost:3000/guardados/" + carId;
              xhr.open('POST', favUrl);
              xhr.setRequestHeader("content-type", "application/json");
              xhr.send(JSON.stringify(autos[i]));
            } else {
              let deleteUrl = "http://localhost:3000/guardados/" + carId;
              xhr.open('DELETE', deleteUrl);
              xhr.setRequestHeader("content-type", "application/json");
              xhr.send();
            }
          });
        }
      }
 
    } else {
      console.log("Error en la base de datos");
    }
  }
  xhr.send();
});
