const productsPerPage = 4;
let currentPage = 1;

function loadProducts(page) {
  let xhr = new XMLHttpRequest();
  let url = "/products/";
  xhr.open('GET', url);
  xhr.setRequestHeader("content-type", "application/json");
  xhr.onload = function () {
    if (xhr.status === 200) {
      var products = JSON.parse(xhr.responseText);
      console.log(products);
      divProducts.innerHTML = products.slice((page - 1) * productsPerPage, page * productsPerPage).map(prod => `
      <div class="row">
      <!--Contenedor Grid-->
      <div class="col-xl-3 col-lg-3 col-md-4 col-sm-12">
        <div class="cookie-card">
          <img class="card-img-top"
            src="${prod.imagen}"
            alt="${prod.marca} ${prod.modelo}">
          <span class="title">${prod.marca}</span>
          <p class="description">${prod.modelo}</p>
          <p class="description">${prod.tipo}</p>
          <div class="card-footer">
            <div class="card-price">
              <span>$${prod.precio}</span>
            </div>
            <div class="card_actions">
              <button class="card_seller">
                Por: ${prod.vendedor}
              </button>
              <Button onclick="ToggleFav(this)" fav-data-id="1" class="card_btn_fav"><i
                  class="fas fa-star"></i></Button>
              <a href="auto.html"><button type="button" class="card_buy_car_btn">Comprar</button></a>
            </div>
          </div>
        </div>
      </div>
        `).join("");

      const pagination = document.querySelector('.pagination');
      const pages = pagination.querySelectorAll('.page-item');
      pages.forEach((pageEl, i) => {
        if (i === page - 1) {
          pageEl.classList.add('active');
        } else {
          pageEl.classList.remove('active');
        }
      });
    } else {
      console.log("Error en la base de datos");
    }
  };
  xhr.send();
}

loadProducts(currentPage);

const previousPageButton = document.getElementById('previous-page');
const nextPageButton = document.getElementById('next-page');

previousPageButton.addEventListener('click', () => {
  if (currentPage > 1) {
    currentPage--;
    loadProducts(currentPage);
  }
});

nextPageButton.addEventListener('click', () => {
  currentPage++;
  loadProducts(currentPage);
});
