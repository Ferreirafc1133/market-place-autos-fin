let url = "http://localhost:3000/products";
let xhr = new XMLHttpRequest();

let testobj = {
    marca : document.getElementById("form-brand").value,
    tipo: document.getElementById("form-type").value,
    modelo: document.getElementById("form-model").value,
    color: document.getElementById("form-color").value,
    precio: document.getElementById("form-price").value,
    año: 2023,
    image: "https://cdn.carbuzz.com/gallery-images/2017-toyota-highlander-hybrid-carbuzz-391672-1600.jpg",
    id: "3623565"
}

document.getElementById('publish-btn').addEventListener('click', async () => {
    xhr.open('POST', url, true);  
    xhr.setRequestHeader('Content-type', 'application/json');
    console.log("Enviando formulario...");
    await xhr.send(JSON.stringify(testobj));
    console.log("Formulario enviado");
});