//Cambio de color boton favs tarjetas


   function ToggleFav(btn) {
    var id = btn.getAttribute("fav-data-id");
    var card_favs_btn_var = document.querySelector("[fav-data-id='" + id + "']");
    
    if (card_favs_btn_var.style.color == "gold") {
        card_favs_btn_var.style.color = "grey";
    } else {
        card_favs_btn_var.style.color = "gold";
    }
  }